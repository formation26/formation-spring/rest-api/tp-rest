package com.example.application;

public interface IMoteurCalcul {

    double sum(double a, double b);

    double times(double a,double b);

    double minus(double a, double b);

    double square(double a);

    double divided(double a,double b);



}
