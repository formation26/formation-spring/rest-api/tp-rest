package com.example.application;

import org.springframework.stereotype.Service;

@Service
public class MoteurCalculImpl implements IMoteurCalcul {
    @Override
    public double sum(double a, double b) {
        return a+b;
    }

    @Override
    public double times(double a, double b) {
        return a*b;
    }

    @Override
    public double minus(double a, double b) {

        return a-b;
    }

    @Override
    public double square(double a) {
        return a*a;
    }

    @Override
    public double divided(double a, double b) {

        return a/b;
    }
}
