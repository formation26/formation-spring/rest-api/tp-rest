package com.example.exposition;

import com.example.application.DiceService;
import com.example.application.DiceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dice")
public class DiceController {


    //DiceService service=new DiceServiceImpl();
    //Demande à Spring de me fournir l'implementation new DiceServiceImpl()

    @Autowired
    DiceService service;
    @GetMapping
    public int launch(){
        return service.launch();
    }
}
