package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class Tp1SpringApplication {

	public static void main(String[] args) {

		SpringApplication.run(Tp1SpringApplication.class, args);
	}

}
