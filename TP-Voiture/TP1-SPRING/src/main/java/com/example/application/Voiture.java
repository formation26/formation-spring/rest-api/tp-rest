package com.example.application;

import java.util.List;

public class Voiture {

    public Voiture(int id, String couleur, List<String> equipements, String nom) {
        this.id = id;
        this.couleur = couleur;
        this.equipements = equipements;
        this.nom = nom;
    }

    private int id;

    private String couleur;

    private List<String> equipements;

    private String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public List<String> getEquipements() {
        return equipements;
    }

    public void setEquipements(List<String> equipements) {
        this.equipements = equipements;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
