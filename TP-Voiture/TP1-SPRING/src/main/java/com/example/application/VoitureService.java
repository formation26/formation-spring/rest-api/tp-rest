package com.example.application;

public interface VoitureService {

    String reparerVoiture();

    Voiture findVoiture(int id);
}
