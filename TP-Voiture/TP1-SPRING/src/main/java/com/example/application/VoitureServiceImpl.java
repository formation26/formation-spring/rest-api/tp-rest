package com.example.application;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VoitureServiceImpl implements VoitureService{
    @Override
    public String reparerVoiture() {
        return "ok";
    }

    @Override
    public Voiture findVoiture(int id) {
        //faire appel la couche infra
        List<String> equipements=new ArrayList<>();
        equipements.add("clim");
        equipements.add("toit ouvrants");

        Voiture v=new Voiture(id,"blanche",equipements,"twingo");

        return  v;
    }
}
