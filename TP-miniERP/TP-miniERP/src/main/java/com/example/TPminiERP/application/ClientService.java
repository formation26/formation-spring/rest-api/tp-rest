package com.example.TPminiERP.application;

import com.example.TPminiERP.domaine.Client;

import java.util.List;

public interface ClientService {

    void create(Client c);

    List<Client> findAll();

    Client findById(int id);
}
