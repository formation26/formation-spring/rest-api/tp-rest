package com.example.TPminiERP.application;

import com.example.TPminiERP.domaine.Adresse;
import com.example.TPminiERP.domaine.Client;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {
    @Override
    public void create(Client c) {

    }

    @Override
    public List<Client> findAll() {

        Client client;

        List<Client> listeClient=new ArrayList<>();
        for(int i=0; i<10;i++){
            Adresse adr=new Adresse("victor hugo"+i,"Toulouse", "31000","FRANCE");
            client =new Client();
            client.setAdresse(adr);
            client.setEmail("email"+i);
            client.setNom("nom"+i);
            client.setDateNaissance("19/05/197"+i);
            listeClient.add(client);
        }
        return listeClient;
    }

    @Override
    public Client findById(int id) {
        Adresse adr=new Adresse("rue de la mairie","Toulouse", "31000","FRANCE");
        Client client =new Client();
        client.setAdresse(adr);
        client.setEmail("durand.martin@organisation.fr");
        client.setNom("durand");
        client.setDateNaissance("19/05/1970");
        return client;
    }
}
