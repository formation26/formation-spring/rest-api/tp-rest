package com.example.TPminiERP.exposition;

import com.example.TPminiERP.application.ClientService;
import com.example.TPminiERP.domaine.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/client")
public class ClientController {

    @Autowired
    ClientService service;
    @PostMapping
    void create(Client c){

    }

    @GetMapping("/all")
    List<Client> findAll(){
        return service.findAll();
    }

    @GetMapping("/{id}")
    Client findById(@PathVariable("id") int id){
        return service.findById(id);
    }
}
